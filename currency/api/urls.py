from django.urls import path
from django.urls import include
from rest_framework.routers import SimpleRouter

from api.views import CompaniesDataViewSet, CompanyViewSet

app_name = "api"

router = SimpleRouter()
router.register(r'companies', CompanyViewSet)
router.register(r'companies_data', CompaniesDataViewSet)


urlpatterns = [
    path('', include((router.urls, "api"), namespace='api')),
    path('companies/',
         CompanyViewSet.as_view({'get': 'list'}),
         name='companies-list'),
    path('currencies/',
         CompaniesDataViewSet.as_view({'get': 'list'}),
         name='companies-data-list'),
]