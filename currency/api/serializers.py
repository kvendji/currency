from rest_framework import serializers
from core.models import Company, CurrencyData


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class CurrencyDataSerializer(serializers.ModelSerializer):
    company = CompanySerializer()

    class Meta:
        model = CurrencyData
        fields = '__all__'
