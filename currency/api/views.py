from rest_framework.viewsets import ModelViewSet

from api.serializers import CompanySerializer, CurrencyDataSerializer
from core.models import CurrencyData, Company


class CompanyViewSet(ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class CompaniesDataViewSet(ModelViewSet):
    queryset = CurrencyData.objects.all()
    serializer_class = CurrencyDataSerializer
    filterset_fields = ['company']
