from django.core.management.base import BaseCommand

from core.parsers import get_currencies


class Command(BaseCommand):

    def handle(self, *args, **options):
        get_currencies()
