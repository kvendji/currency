from django.contrib import admin
from core.models import CurrencyData, Company

admin.site.register(CurrencyData)
admin.site.register(Company)
