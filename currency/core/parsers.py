from datetime import datetime, timedelta

import requests

from core.models import CurrencyData, Company
from currency import settings


def get_currencies():
    current_datetime = datetime.now() + timedelta(days=1)
    timestamp = int(datetime.timestamp(current_datetime))
    for company in settings.LIST_OF_COMPANIES:
        company_obj, created = Company.objects.get_or_create(short_name=company, name='')
        url = f'https://query1.finance.yahoo.com/v7/finance/download/{company}?period1=0&period2={timestamp}&interval=1d&events=history&includeAdjustedClose=true' # noqa
        response = requests.get(url)
        if response.status_code == 200:
            data = response.content
            data_str = bytes.decode(data, encoding='utf-8').split('\n')[1:]
            currency_lst = []
            for row in data_str:
                row_lst = row.split(',')
                current_date = datetime.strptime(row_lst[0], '%Y-%m-%d').date()
                last_date_info = CurrencyData.objects.filter(company=company_obj).last().date
                if last_date_info >= current_date:
                    continue
                else:
                    currency_lst.append(CurrencyData(
                        company=company_obj,
                        date=current_date,
                        open=row_lst[1],
                        high=row_lst[2],
                        low=row_lst[3],
                        close=row_lst[4],
                        adj_close=row_lst[5],
                        volume=row_lst[6]
                    ))
            CurrencyData.objects.bulk_create(currency_lst)
