from currency import celery_app
from core.parsers import get_currencies


@celery_app.task
def get_currencies_task():
    get_currencies()
