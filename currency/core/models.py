from django.db import models


class Company(models.Model):

    name = models.CharField(max_length=255, blank=True, default='')
    short_name = models.CharField(max_length=20, blank=True, default='')

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return f'{self.name}({self.short_name})'


class CurrencyData(models.Model):

    company = models.ForeignKey('Company', on_delete=models.SET_NULL,
                                related_name='companies',
                                null=True, blank=True,
                                verbose_name="Data of company")
    date = models.DateField('Date', default=None,
                            null=True, blank=True, db_index=True)
    open = models.CharField('Open', max_length=255, blank=True)
    high = models.CharField('High	', max_length=255, blank=True)
    low = models.CharField('Low', max_length=255, blank=True)
    close = models.CharField('Close', max_length=255, blank=True)
    adj_close = models.CharField('Adj Close', max_length=255, blank=True)
    volume = models.CharField('Volume', max_length=255, blank=True)

    class Meta:
        verbose_name = 'Currency stat'
        ordering = ['date']
